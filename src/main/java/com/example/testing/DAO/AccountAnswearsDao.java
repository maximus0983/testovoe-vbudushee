package com.example.testing.DAO;

import com.example.testing.common.AccountAnswears;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountAnswearsDao {
    private static final String SELECT_ALL = "select * from accountAnswears";
    private static final String SELECT_BY_ACCOUNTID = SELECT_ALL + " where accId = ?";
    private static final String INSERT = "insert into accountAnswears(accId, qaId, answear, rightAnswear) " +
            "values(?, ?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AccountAnswearsDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(int accId, int qaId, String answear, boolean rightAnswear) {
        jdbcTemplate.update(INSERT, accId, qaId, answear, rightAnswear);
    }

    public List<AccountAnswears> getByAccId(int accId) {
        return jdbcTemplate.query(SELECT_BY_ACCOUNTID, new ResultSetExtractor<List<AccountAnswears>>() {
            @Nullable
            @Override
            public List<AccountAnswears> extractData(ResultSet rs) throws SQLException, DataAccessException {
                List<AccountAnswears> accountAnswears = new ArrayList<>();
                while (rs.next()) {
                    accountAnswears.add(createAccountAnswear(rs));
                }
                return accountAnswears;
            }
        }, accId);
    }

    private AccountAnswears createAccountAnswear(ResultSet rs) throws SQLException {
        AccountAnswears accountAnswears = new AccountAnswears();
        accountAnswears.setAccId(rs.getInt("accId"));
        accountAnswears.setQaId(rs.getInt("qaId"));
        accountAnswears.setAnswear(rs.getString("answear"));
        accountAnswears.setRightAnswear(rs.getBoolean("rightAnswear"));
        return accountAnswears;
    }
}