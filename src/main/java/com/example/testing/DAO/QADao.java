package com.example.testing.DAO;

import com.example.testing.common.QuestionAnswears;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class QADao {
    private static final String SELECT_ALL = "select * from qa";
    private static final String SELECT_BY_ID = SELECT_ALL + " where id = ?";
    private static final String INSERT = "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
            "rightAnswear) values(?, ?, ?, ?, ?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public QADao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(QuestionAnswears qa) {
        jdbcTemplate.update(INSERT, qa.getQuestion(), qa.getAnswears().get(0), qa.getAnswears().get(1),
                qa.getAnswears().get(2), qa.getAnswears().get(3), qa.getAnswears().get(4), qa.getRightAnswear());
    }

    public List<QuestionAnswears> getAll() {
        return jdbcTemplate.query(SELECT_ALL, new ResultSetExtractor<List<QuestionAnswears>>() {
            @Override
            public List<QuestionAnswears> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<QuestionAnswears> questionAnswears = new ArrayList<>();
                while (resultSet.next()) {
                    questionAnswears.add(createQA(resultSet));
                }
                return questionAnswears;
            }
        });
    }

    public QuestionAnswears getById(int id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_BY_ID, new Object[]{id},
                    new RowMapper<QuestionAnswears>() {
                        @Nullable
                        @Override
                        public QuestionAnswears mapRow(ResultSet resultSet, int i) throws SQLException {
                            return createQA(resultSet);
                        }
                    });
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private QuestionAnswears createQA(ResultSet rs) {
        QuestionAnswears qa = new QuestionAnswears();
        try {
            qa.setId(rs.getInt("id"));
            qa.setQuestion(rs.getString("question"));
            qa.setRightAnswear(rs.getString("rightAnswear"));
            List<String> answears = new ArrayList<>(4);
            for (int i = 1; i < 5; i++) {
                answears.add(rs.getString("answear" + i));
            }
            qa.setAnswears(answears);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return qa;
    }
}
