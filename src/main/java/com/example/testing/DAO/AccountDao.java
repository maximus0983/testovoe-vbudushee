package com.example.testing.DAO;

import com.example.testing.common.Account;
import com.example.testing.common.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class AccountDao {
    private static final String SELECT_ALL = "select * from account";
    private static final String INSERT = "insert into account(username,role, password) values(?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AccountDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Account findByUsername(String username) {
        try {
            return jdbcTemplate.queryForObject(SELECT_ALL + " where username = ?", new Object[]{username},
                    new RowMapper<Account>() {
                        @Nullable
                        @Override
                        public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                            return createAccount(resultSet);
                        }
                    });
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static Account createAccount(ResultSet rs) throws SQLException {
        Account account = new Account();
        account.setId(rs.getInt("id"));
        account.setUsername(rs.getString("username"));
        account.setRole(Role.valueOf(rs.getString("role")));
        account.setPassword(rs.getString("password"));
        return account;
    }

    public void save(Account account) {
        jdbcTemplate.update(INSERT, account.getUsername(), account.getRole().toString(), account.getPassword());
    }
}