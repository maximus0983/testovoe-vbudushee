package com.example.testing.common;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountAnswears {
    private int accId;
    private int qaId;
    private String answear;
    private boolean rightAnswear;

    public AccountAnswears(int accId, int qaId, String answear, boolean rightAnswear) {
        this.accId = accId;
        this.qaId = qaId;
        this.answear = answear;
        this.rightAnswear = rightAnswear;
    }

    public AccountAnswears() {
    }

    public int getAccId() {
        return accId;
    }

    public void setAccId(int accId) {
        this.accId = accId;
    }

    public int getQaId() {
        return qaId;
    }

    public void setQaId(int qaId) {
        this.qaId = qaId;
    }

    public String getAnswear() {
        return answear;
    }

    public void setAnswear(String answear) {
        this.answear = answear;
    }

    public boolean isRightAnswear() {
        return rightAnswear;
    }

    public void setRightAnswear(boolean rightAnswear) {
        this.rightAnswear = rightAnswear;
    }
}
