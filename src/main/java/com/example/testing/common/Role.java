package com.example.testing.common;

public enum Role {
    ADMIN,
    USER;

    Role() {
    }
}
