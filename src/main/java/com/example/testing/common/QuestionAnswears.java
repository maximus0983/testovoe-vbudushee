package com.example.testing.common;

import java.util.List;

public class QuestionAnswears {
    private int id;
    private String question;
    private List<String> answears;
    private String rightAnswear;

    public QuestionAnswears(String question, List<String> answears, String rightAnswear) {
        this.question = question;
        this.answears = answears;
        this.rightAnswear = rightAnswear;
    }

    public QuestionAnswears() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getAnswears() {
        return answears;
    }

    public void setAnswears(List<String> answears) {
        this.answears = answears;
    }

    public String getRightAnswear() {
        return rightAnswear;
    }

    public void setRightAnswear(String rightAnswear) {
        this.rightAnswear = rightAnswear;
    }

    @Override
    public String toString() {
        return "QuestionAnswears{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answears=" + answears.toString() +
                ", rightAnswear='" + rightAnswear + '\'' +
                '}';
    }
}
