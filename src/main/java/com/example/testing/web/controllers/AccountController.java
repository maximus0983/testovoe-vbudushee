package com.example.testing.web.controllers;

import com.example.testing.common.Account;
import com.example.testing.common.CustomUserDetails;
import com.example.testing.common.Role;
import com.example.testing.service.AccountService;
import com.example.testing.service.QAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccountController {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AccountService accountService;
    @Autowired
    private QAService qaService;

    @RequestMapping("/")
    public String showLoginPage(Model model) {
        model.addAttribute("questions", qaService.getAll());
        return "homepage";
    }

    @GetMapping("/registration")
    public String showRegistrationForm() {
        return "registration";
    }

    @GetMapping("/login")
    public String viewHomePage() {
        return "loginpage";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "loginpage";
    }

    @PostMapping("/regController")
    public String registerUserAccount(@ModelAttribute Account account,
                                      BindingResult result) {

        Account existing = accountService.findByUsername(account.getUsername());
        if (existing != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()) {
            return "registration";
        }
        account.setRole(Role.USER);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        accountService.save(account);
        UserDetails accountForSecurity = new CustomUserDetails(account);
        Authentication auth = new UsernamePasswordAuthenticationToken(accountForSecurity, null,
                accountForSecurity.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}