package com.example.testing.web.controllers;

import com.example.testing.common.QuestionAnswears;
import com.example.testing.service.QAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class QAController {
    private final QAService qaService;

    @Autowired
    public QAController(QAService qaService) {
        this.qaService = qaService;
    }

    @RequestMapping("/createQuestion")
    public String createQuestion(@ModelAttribute QuestionAnswears questionAnswears) {
        qaService.save(questionAnswears);
        return "homepage";
    }
}
