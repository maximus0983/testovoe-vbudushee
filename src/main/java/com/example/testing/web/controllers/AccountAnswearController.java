package com.example.testing.web.controllers;

import com.example.testing.common.AccountAnswears;
import com.example.testing.common.CustomUserDetails;
import com.example.testing.common.QuestionAnswears;
import com.example.testing.service.AccountAnswearsService;
import com.example.testing.service.QAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AccountAnswearController {
    private final AccountAnswearsService accountAnswearsService;
    private final QAService qaService;

    @Autowired
    public AccountAnswearController(AccountAnswearsService accountAnswearsService, QAService qaService) {
        this.accountAnswearsService = accountAnswearsService;
        this.qaService = qaService;
    }


    @RequestMapping("/test")
    public String SaveAnswear(@RequestParam("answear") String[] answears, @RequestParam("question") int[] questions) {
        int accountId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            accountId = ((CustomUserDetails) principal).getUser().getId();
        } else {
            accountId = -1;
        }
        List<QuestionAnswears> qa = qaService.getAll();
        for (int i = 0; i < answears.length; i++) {
            boolean rightAnswear = qa.get(i).getRightAnswear().equals(answears[i]);
            accountAnswearsService.save(accountId, questions[i], answears[i], rightAnswear);
        }
        return "results";
    }

    @RequestMapping("/showResults")
    @ResponseBody
    public List<AccountAnswears> showResults() {
        int accountId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            accountId = ((CustomUserDetails) principal).getUser().getId();
        } else {
            accountId = -1;
        }
        return accountAnswearsService.getByAccountId(accountId);
    }
}