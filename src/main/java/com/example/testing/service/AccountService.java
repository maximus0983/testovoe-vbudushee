package com.example.testing.service;

import com.example.testing.DAO.AccountDao;
import com.example.testing.common.Account;
import com.example.testing.common.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountService {
    private final AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public UserDetails getByUsername(String username) {
        Account account = accountDao.findByUsername(username);
        return new CustomUserDetails(account);
    }

    public Account findByUsername(String username) {
        return accountDao.findByUsername(username);
    }

    @Transactional
    public void save(Account account) {
        accountDao.save(account);
    }
}
