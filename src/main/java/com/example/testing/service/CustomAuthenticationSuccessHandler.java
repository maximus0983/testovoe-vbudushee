package com.example.testing.service;

import com.example.testing.common.Account;
import com.example.testing.common.CustomUserDetails;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails;
        if (auth instanceof AnonymousAuthenticationToken) customUserDetails = null;
        else customUserDetails = (CustomUserDetails) auth.getPrincipal();
        Account account = customUserDetails == null ? null : customUserDetails.getUser();
        System.out.println("principal " + auth.getPrincipal());
    }
}
