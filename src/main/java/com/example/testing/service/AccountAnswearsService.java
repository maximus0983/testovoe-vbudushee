package com.example.testing.service;

import com.example.testing.DAO.AccountAnswearsDao;
import com.example.testing.common.AccountAnswears;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountAnswearsService {
    private final AccountAnswearsDao accountAnswearsDao;

    @Autowired
    public AccountAnswearsService(AccountAnswearsDao accountAnswearsDao) {
        this.accountAnswearsDao = accountAnswearsDao;
    }

    @Transactional
    public void save(int accId, int qaId, String answear, boolean rightAnswear) {
        accountAnswearsDao.save(accId, qaId, answear, rightAnswear);
    }

    @Transactional(readOnly = true)
    public List<AccountAnswears> getByAccountId(int accId) {
        return accountAnswearsDao.getByAccId(accId);
    }
}
