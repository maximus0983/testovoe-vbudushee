package com.example.testing.service;

import com.example.testing.DAO.QADao;
import com.example.testing.common.QuestionAnswears;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class QAService {
    private final QADao qaDao;

    @Autowired
    public QAService(QADao qaDao) {
        this.qaDao = qaDao;
    }

    public List<QuestionAnswears> getAll() {
        return qaDao.getAll();
    }

    public void save(QuestionAnswears qa) {
        qaDao.save(qa);
    }
}