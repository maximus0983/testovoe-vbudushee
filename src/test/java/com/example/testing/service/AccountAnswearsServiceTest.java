package com.example.testing.service;

import com.example.testing.DAO.AccountAnswearsDao;
import com.example.testing.common.AccountAnswears;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
class AccountAnswearsServiceTest {
    @Autowired
    AccountAnswearsService accountAnswearsService=new AccountAnswearsService(accountAnswearsDao);
//    @MockBean
    private static AccountAnswearsDao accountAnswearsDao=mock(AccountAnswearsDao.class);

    @Test
    void getByAccountId() {
        AccountAnswears acan1 = new AccountAnswears(1, 1, "1", false);
        AccountAnswears acan2 = new AccountAnswears(1, 2, "2", true);
        List<AccountAnswears> list = new ArrayList<>();
        list.add(acan1);
        list.add(acan2);
        when(accountAnswearsDao.getByAccId(1)).thenReturn(list);
        assertTrue(accountAnswearsService.getByAccountId(1).get(1).isRightAnswear());
    }
}