package com.example.testing.service;

import com.example.testing.DAO.AccountDao;
import com.example.testing.common.Account;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
class AccountServiceTest {
    private static AccountDao accountDao=mock(AccountDao.class) ;

    AccountService accountService=new AccountService(accountDao);


    @Test
    void findByUsername() {
        Account account=new Account("vasya","1");
        Mockito.when(accountDao.findByUsername("vasya")).thenReturn(account);
        assertEquals("vasya",accountService.findByUsername("vasya").getUsername());
    }
}