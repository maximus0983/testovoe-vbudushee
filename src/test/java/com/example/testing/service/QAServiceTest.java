package com.example.testing.service;

import com.example.testing.DAO.QADao;
import com.example.testing.common.QuestionAnswears;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
class QAServiceTest {
    private static QADao qaDao=mock(QADao.class);

    QAService qaService=new QAService(qaDao);

    @Test
    void getAll() {
        List<QuestionAnswears> questionAnswears=new ArrayList<>();
        questionAnswears.add(new QuestionAnswears("what?", Arrays.asList("1","2","3","4","5"),"5"));
        questionAnswears.add(new QuestionAnswears("what's Up?", Arrays.asList("1","2","3","4","5"),"4"));
        when(qaDao.getAll()).thenReturn(questionAnswears);
        assertEquals(2,qaService.getAll().size());
    }
}