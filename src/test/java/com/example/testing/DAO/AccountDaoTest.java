package com.example.testing.DAO;

import com.example.testing.common.Account;
import com.example.testing.common.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
class AccountDaoTest {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    AccountDao accountDao;

    @BeforeEach
    void setUp() {
        jdbcTemplate.execute("CREATE TABLE account" +
                "(" +
                "     id int PRIMARY KEY AUTO_INCREMENT," +
                "     username varchar(255)," +
                "     password varchar(255)," +
                "     role varchar(255)" +
                " );");
        jdbcTemplate.execute("insert into account(username,role, password) values('vasya','ADMIN' ,'1' )");
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("drop table account");
    }

    @Test
    void findByUsername() {
        assertEquals("vasya",accountDao.findByUsername("vasya").getUsername());
    }

    @Test
    void save() {
        Account account=new Account("petya","1");
        account.setRole(Role.USER);
        accountDao.save(account);
        assertEquals("petya",accountDao.findByUsername("petya").getUsername());
    }
}