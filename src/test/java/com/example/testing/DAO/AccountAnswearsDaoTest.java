package com.example.testing.DAO;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
class AccountAnswearsDaoTest {
    @Autowired
    AccountAnswearsDao accountAnswearsDao;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        jdbcTemplate.execute("CREATE TABLE account" +
                "(" +
                "     id int PRIMARY KEY AUTO_INCREMENT," +
                "     username varchar(255)," +
                "     password varchar(255)," +
                "     role varchar(255)" +
                " );");
        jdbcTemplate.execute("insert into account(username,role, password) values('vasya','ADMIN' ,'1' )");
        jdbcTemplate.execute("CREATE TABLE QA (id int AUTO_INCREMENT PRIMARY KEY," +
                "    question varchar(500)," +
                "    answear1 varchar(500)," +
                "    answear2 varchar(500)," +
                "    answear3 varchar(500)," +
                "    answear4 varchar(500)," +
                "    answear5 varchar(500)," +
                "    rightAnswear varchar(500)" +
                ");");
        jdbcTemplate.update("insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('сколько', '1', '2', '3', '4', '5', '1');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('когда', '1', '2', '3', '4', '5', '2');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('по чем', '1', '2', '3', '4', '5', '3');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('с кем', '1', '2', '3', '4', '5', '4');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('высота', '1', '2', '3', '4', '5', '5');");
        jdbcTemplate.execute("CREATE TABLE accountAnswears" +
                " (" +
                "     accId int NOT NULL," +
                "     qaId int NOT NULL," +
                "     answear varchar(500)," +
                "     rightAnswear boolean," +
                "     CONSTRAINT accountAnswears_account_id_fk FOREIGN KEY (accId) REFERENCES account (id)," +
                "     CONSTRAINT accountAnswears_QA_id_fk FOREIGN KEY (qaId) REFERENCES QA (id)" +
                " );");
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("drop table qa");
        jdbcTemplate.execute("drop table accountAnswears");
        jdbcTemplate.execute("drop table account");
    }

    @Test
    void save() {
        accountAnswearsDao.save(1,1,"1",true);
        assertEquals("1",accountAnswearsDao.getByAccId(1).get(0).getAnswear());
    }

    @Test
    void getByAccId() {
        accountAnswearsDao.save(1,1,"1",true);
        accountAnswearsDao.save(1,2,"3",false);
        assertEquals(false,accountAnswearsDao.getByAccId(1).get(1).isRightAnswear());
    }
}