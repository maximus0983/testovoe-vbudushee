package com.example.testing.DAO;

import com.example.testing.common.QuestionAnswears;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@JdbcTest
@ComponentScan
class QADaoTest {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    QADao qaDao;

    @BeforeEach
    void setUp() {
        jdbcTemplate.execute("CREATE TABLE QA (id int AUTO_INCREMENT PRIMARY KEY," +
                "    question varchar(500)," +
                "    answear1 varchar(500)," +
                "    answear2 varchar(500)," +
                "    answear3 varchar(500)," +
                "    answear4 varchar(500)," +
                "    answear5 varchar(500)," +
                "    rightAnswear varchar(500)" +
                ");");
        jdbcTemplate.update("insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('сколько', '1', '2', '3', '4', '5', '2');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('когда', '1', '2', '3', '4', '5', '2');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('по чем', '1', '2', '3', '4', '5', '2');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('с кем', '1', '2', '3', '4', '5', '2');" +
                "insert into qa(question,answear1,answear2,answear3,answear4,answear5," +
                "            rightAnswear) values('высота', '1', '2', '3', '4', '5', '2');");
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.execute("drop table QA");
    }

    @Test
    void save() {
        List<String> answears = Arrays.asList("1","2","3","4","5");
        qaDao.save(new QuestionAnswears("why",answears,answears.get(0)));
        assertEquals("why",qaDao.getById(6).getQuestion());
    }

    @Test
    void getAll() {
        assertEquals(5,qaDao.getAll().size());
    }

    @Test
    void getById() {
        assertEquals("сколько", qaDao.getById(1).getQuestion());
    }
}